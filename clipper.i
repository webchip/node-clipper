%module clipper
%{
#include "clipper.hpp"
%}

%include "std_except.i"
%include "std_vector.i"

%include "clipper.hpp"

namespace ClipperLib
{
  %template(Path)  std::vector<IntPoint>;
  %template(Paths) std::vector<Path>;
}