var http = require('http');
var fs = require('fs');
var url = require('url');
var path = require('path');
var exec = require('child_process').exec;
var log = require('util').log;
var assert = require('assert');


function downloadFile(downloadUrl, callback) {
    log('download: ' + downloadUrl);
    var parsedUrl = url.parse(downloadUrl);
    var filePath = __dirname + '/'  + path.basename(parsedUrl.pathname);

    var file = fs.createWriteStream(filePath);
    http.get(parsedUrl, function(res){
        log('save to: ' + filePath);
        res.on('end', callback);
        res.pipe(file);
    });
}

log('work directory: ' + __dirname);

var v8 = process.versions.v8.split('.');
v8 = '0x0' + v8[0] + v8[1] + v8[2];

log('v8 version: ' + v8);

downloadFile('http://sourceforge.net/p/polyclipping/code/HEAD/tree/trunk/cpp/clipper.cpp?format=raw', function(){
    downloadFile('http://sourceforge.net/p/polyclipping/code/HEAD/tree/trunk/cpp/clipper.hpp?format=raw', function(){
        log('run swig');
        exec('swig -DV8_VERSION=' + v8 + ' -javascript -node -c++ ' + __dirname + '/clipper.i',
            function (error, stdout, stderr) {
                assert.ifError(error);
            });
    });
});




